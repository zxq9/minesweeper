%%% @doc
%%% Minesweeper Controller
%%%
%%% This process is a in charge of maintaining the program's state.
%%% @end

-module(ms_con).
-vsn("1.1.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-behavior(gen_server).
-export([new_game/0, clicked/1, get_conf/0, set_conf/2]).
-export([start_link/0, stop/0]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {window     = none     :: none | wx:wx_object(),
         dimensions = {10, 10} :: {pos_integer(), pos_integer()},
         density    = 0.15     :: float(),
         grid       = none     :: none | [square()]}).

-record(square,
        {coord  = {0, 0}    :: coord(),
         type   = empty     :: empty | mine | {adjacent, 1..8} | assplode,
         status = unclicked :: unclicked | clicked}).


-type coord()  :: {X :: integer(), Y :: integer()}.
-type square() :: #square{}.



%% Interface

new_game() ->
    gen_server:cast(?MODULE, new_game).


clicked(Coord) ->
    gen_server:cast(?MODULE, {clicked, Coord}).


get_conf() ->
    gen_server:call(?MODULE, get_conf).


set_conf(Dimensions, Density) ->
    gen_server:cast(?MODULE, {set_conf, Dimensions, Density}).


-spec stop() -> ok.

stop() ->
    gen_server:cast(?MODULE, stop).



%%% Startup Functions


-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: {already_started, pid()}
                 | {shutdown, term()}
                 | term().
%% @private
%% Called by ms_sup.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> no_return().

init(none) ->
    ok = log(info, "Starting"),
    Window = ms_gui:start_link("Minesweeper"),
    Dimensions = {10, 10},
    State = #s{window = Window, dimensions = Dimensions},
    ok = ms_gui:new_grid(Dimensions),
    {ok, State}.



%%% gen_server


handle_call(get_conf, _, State = #s{dimensions = Dimensions, density = Density}) ->
    Response = {Dimensions, Density},
    {reply, Response, State};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


handle_cast({clicked, Coord}, State) ->
    NewState = do_clicked(Coord, State),
    {noreply, NewState};
handle_cast(new_game, State = #s{dimensions = Dimensions}) ->
    ok = log(info, "Starting new game."),
    ok = ms_gui:new_grid(Dimensions),
    {noreply, State#s{grid = none}};
handle_cast({set_conf, Dimensions, Density}, State) ->
    ok = log(info, "Starting new game with new configuration."),
    ok = ms_gui:new_grid(Dimensions),
    {noreply, State#s{grid = none, dimensions = Dimensions, density = Density}};
handle_cast(stop, State) ->
    ok = log(info, "Received a 'stop' message."),
    {stop, normal, State};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    zx:stop().


%%% Doers

do_clicked(Coord,
           State = #s{grid = none, dimensions = Dimensions, density = Density}) ->
    {MineCount, Grid} = gen_grid(Coord, Dimensions, Density),
    ok = ms_gui:mine_total(MineCount),
    do_clicked(Coord, State#s{grid = Grid});
do_clicked(Coord, State = #s{grid = Grid, dimensions = Dimensions}) ->
    case lists:keyfind(Coord, #square.coord, Grid) of
        Clicked = #square{status = unclicked, type = empty} ->
            {Uncovered, NewGrid} = uncover(Clicked, Coord, Grid, Dimensions),
            ok = send_update(Uncovered),
            NewState = State#s{grid = NewGrid},
            ok = check_win(NewState),
            NewState;
        Clicked = #square{status = unclicked, type = mine} ->
            Updated = Clicked#square{type = assplode},
            NewGrid = lists:keystore(Coord, #square.coord, Grid, Updated),
            Final = [T#square{status = clicked} || T <- NewGrid],
            ok = send_update(Final),
            ok = ms_gui:you_died(),
            State#s{grid = Final};
        Clicked = #square{status = unclicked} ->
            Updated = Clicked#square{status = clicked},
            NewGrid = lists:keystore(Coord, #square.coord, Grid, Updated),
            ok = send_update([Updated]),
            NewState = State#s{grid = NewGrid},
            ok = check_win(NewState),
            NewState;
        #square{status = clicked} ->
            State
    end.


check_win(#s{grid = Grid}) ->
    Clear = fun(#square{type = T, status = S}) -> S == clicked orelse T == mine end,
    case lists:all(Clear, Grid) of
        true ->
            Final = [T#square{status = clicked} || T <- Grid],
            ok = send_update(Final),
            ms_gui:you_won();
        false ->
            ok
    end.


uncover(Clicked, Coord, Grid, Dimensions) ->
    Area = ordsets:from_list(search_area(Coord, Dimensions)),
    Scrubbed = lists:keydelete(Coord, #square.coord, Grid),
    Original = Clicked#square{status = clicked},
    NewGrid = lists:keystore(Coord, #square.coord, Grid, Original),
    Uncovered = [Original],
    uncover(Area, Uncovered, Scrubbed, NewGrid, Dimensions).

uncover([Coord | Rest], Uncovered, Scrubbed, Grid, Dimensions) ->
    case lists:keytake(Coord, #square.coord, Scrubbed) of
        {value, #square{status = clicked}, NewScrubbed} ->
            uncover(Rest, Uncovered, NewScrubbed, Grid, Dimensions);
        {value, Selected = #square{status = unclicked, type = empty}, NewScrubbed} ->
            Updated = Selected#square{status = clicked},
            Additional = search_area(Coord, Dimensions),
            NewArea = lists:foldl(fun ordsets:add_element/2, Rest, Additional),
            NewUncovered = [Updated | Uncovered],
            NewGrid = lists:keystore(Coord, #square.coord, Grid, Updated),
            uncover(NewArea, NewUncovered, NewScrubbed, NewGrid, Dimensions);
        {value, Selected = #square{status = unclicked}, NewScrubbed} ->
            Updated = Selected#square{status = clicked},
            NewUncovered = [Updated | Uncovered],
            NewGrid = lists:keystore(Coord, #square.coord, Grid, Updated),
            uncover(Rest, NewUncovered, NewScrubbed, NewGrid, Dimensions);
        false ->
            uncover(Rest, Uncovered, Scrubbed, Grid, Dimensions)
    end;
uncover([], Uncovered, _, Grid, _) ->
    {Uncovered, Grid}.

gen_grid(Protected, Dimensions = {X, Y}, Density) ->
    Mined = gen_grid(X, Y, 0, [], Density),
    S = lists:keyfind(Protected, #square.coord, Mined),
    Safe = lists:keystore(Protected, #square.coord, Mined, S#square{type = empty}),
    Generated = find_adjacent(Safe, Dimensions),
    MineCount = count_mines(Generated, 0),
    {MineCount, Generated}.

count_mines([#square{type = mine} | Rest], Count) ->
    count_mines(Rest, Count + 1);
count_mines([_ | Rest], Count) ->
    count_mines(Rest, Count);
count_mines([], Count) ->
    Count.

gen_grid(MaxX, MaxY, X, Squares, Density) when X < MaxX->
    NewSquares = gen_row(X, MaxY, 0, Squares, Density),
    gen_grid(MaxX, MaxY, X + 1, NewSquares, Density);
gen_grid(_, _, _, Squares, _) ->
    lists:reverse(Squares).

gen_row(X, MaxY, Y, Squares, Density) when Y < MaxY ->
    NewSquares = [#square{coord = {X, Y}, type = maybe_mine(Density)} | Squares],
    gen_row(X, MaxY, Y + 1, NewSquares, Density);
gen_row(_, _, _, Squares, _) ->
    Squares.

maybe_mine(Density) ->
    case rand:uniform() > Density of
        true  -> empty;
        false -> mine
    end.

find_adjacent(Mined, Dimensions) ->
    find_adjacent(Mined, Mined, [], Dimensions).

find_adjacent([S = #square{type = empty, coord = Coord} | Rest],
              Mined, Adjacent, Dimensions) ->
    Type = count_adjacent(Coord, Mined, Dimensions),
    find_adjacent(Rest, Mined, [S#square{type = Type} | Adjacent], Dimensions);
find_adjacent([Square = #square{type = mine} | Rest], Mined, Adjacent, Dimensions) ->
    find_adjacent(Rest, Mined, [Square | Adjacent], Dimensions);
find_adjacent([], _, Adjacent, _) ->
    Adjacent.

count_adjacent(Coord, Mined, Dimensions) ->
    Area = search_area(Coord, Dimensions),
    case adjacent_count(Area, Mined, 0) of
        0 -> empty;
        N -> {adjacent, N}
    end.

adjacent_count([Coord | Rest], Mined, Count) ->
    case lists:keyfind(Coord, #square.coord, Mined) of
        #square{type = empty} -> adjacent_count(Rest, Mined, Count);
        #square{type = mine}  -> adjacent_count(Rest, Mined, Count + 1)
    end;
adjacent_count([], _, Count) ->
    Count.


send_update(Grid) ->
    Board = lists:map(fun square_update/1, Grid),
    ms_gui:update(Board).


square_update(#square{coord = Coord, status = unclicked}) ->
    {Coord, unclicked};
square_update(#square{coord = Coord, status = clicked, type = Type}) ->
    {Coord, Type}.


search_area(Coord = {X, Y}, {MaxX, MaxY}) ->
    Block = [{SX, SY} || SX <- [X -1, X, X +1], SY <- [Y -1, Y, Y + 1],
                         SX >= 0, SY >= 0, SX < MaxX, SY < MaxY],
    lists:delete(Coord, Block).
