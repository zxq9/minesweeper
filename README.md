# minesweeper

An Erlang implementation of the classic logic game minesweeper.

## How to Run It

Minesweeper was developed with and depends on [ZX](https://zxq9.com/projects/zomp/) to launch properly.

If you have ZX installed you can do `zx run minesweeper`, or from the GUI launch [Vapor](https://gitlab.com/zxq9/vapor) and click "Run" next to Minesweeper's entry in the app list (or for maximal fun, do `zx run vapor` and *then* select Minesweeper from the list!).
